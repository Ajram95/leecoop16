%% Arjun Ramaswamy - November 2016
%% Analysis of x-ray microscopy data from Thio stained AD data
%% Find simple circular average of an image from the desired x,y coordinates of spotted plaques.


% clc;  
% close all; 
% clear; 

format long g;
format compact;
fontSize = 20;

%Enter the variable name of the image matrix here
grayImage = D;
grayImage(grayImage==0)=nan;
% Get the dimensions of the image.  
% numberOfColorBands should be = 1.
[rows, columns, numberOfColorChannels] = size(grayImage);
if numberOfColorChannels > 1
	% It's not really gray scale like we expected - it's color.
	grayImage = rgb2gray(grayImage); % Convert to gray.
end

% Display original image.
subplot(2, 2, 1);
imshow(grayImage, []);

%For pointing cursor at region of interest
title('Original Image', 'FontSize', fontSize);
% Set up figure properties:
% Enlarge figure to full screen.
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% Get rid of tool bar and pulldown menus that are along top of figure.
set(gcf, 'Toolbar', 'none', 'Menu', 'none');
drawnow;


% Locate the center
promptMessage = sprintf('Click at the center (where you want the circular average to start)');
titleBarCaption = 'Continue?';
buttonText = questdlg(promptMessage, titleBarCaption, 'OK', 'Quit', 'OK');
if strcmpi(buttonText, 'Quit')
	return;
end
[x,y] = ginput(1);

% %% For manual entry of x,y coordinates
% x = 392;
% y = 392;

% Find out what the max distance will be by computing the distance to each corner.
distanceToUL = sqrt((1-y)^2 + (1-x)^2)
distanceToUR = sqrt((1-y)^2 + (columns-x)^2)
distanceToLL = sqrt((rows-y)^2 + (1-x)^2)
distanceToLR= sqrt((rows-y)^2 + (columns-x)^2)

maxDistance = ceil(max([distanceToUL, distanceToUR, distanceToLL, distanceToLR]))

% Create another display and put a cross at the center and concentric circles around it.
% Display the original image.
subplot(2, 2, 2);
%figure()
imshow(grayImage, []);
axis on;
title('With circles along radius gridlines from plot below', 'FontSize', fontSize);
hold on;
line([x, x], [1, rows], 'Color', 'r', 'LineWidth', 2);
line([1, columns], [y, y], 'Color', 'r', 'LineWidth', 2);

%Compute and display the histogram.
% [pixelCount, grayLevels] = imhist(grayImage);
% subplot(2, 2, 3); 
% bar(grayLevels, pixelCount);
% grid on;
% title('Histogram of Original Image', 'FontSize', fontSize);
% xlim([0 grayLevels(end)]); % Scale x axis manually.
% drawnow; 

% Allocate an array for the profile
profileSums = zeros(1, maxDistance);
profileCounts = zeros(1, maxDistance);
% Scan the original image getting gray level, and scan edtImage getting distance.
% Then add those values to the profile.
for column = 1 : columns
	for row = 1 : rows
		thisDistance = round(sqrt((row-y)^2 + (column-x)^2));
		if thisDistance <= 0
			continue;
		end
		profileSums(thisDistance) = profileSums(thisDistance) + double(grayImage(row, column));
		profileCounts(thisDistance) = profileCounts(thisDistance) + 1;
	end
end

% Divide the sums by the counts at each distance to get the average profile
averageRadialProfile = profileSums ./ profileCounts;
% Plot it.
subplot(2, 2, 3.5); 
plot(1:length(averageRadialProfile), averageRadialProfile, 'b-', 'LineWidth', 3);
xlim([0 200]);
grid on;
title('Circular Average', 'FontSize', fontSize);
xlabel('Distance from center (pixels)', 'FontSize', fontSize);
ylabel('Average Gray Level (Intensity)', 'FontSize', fontSize);

% Have the circles over the image be at the same distances as the grid lines along the x axis.
% Get the tick marks along the x axis
ax = gca;
xTickNumbers = ax.XTick;
xTickNumbers(xTickNumbers == 0) = [];  % Zero is probably in there, so get rid of it.
radii = [0:10:150];
centers = repmat([x, y], length(radii), 1);
subplot(2, 2, 2);  % Switch to upper right image.
hold on;
viscircles(centers,radii); % Plot the circles over the image.