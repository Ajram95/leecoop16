%Analyzing 2500 patterns by color maping
%Human brain tissue (tif) files
%Arjun Ramaswamy 7/8/16
close all; clear all;
tic
%Directory for image files
cd('E:\LeeLab-Coop1\Frozen tissue\sample6\test1');
%Select all 2500 ad patterns
tifFiles = dir('*tif');

set(gca,'FontSize',16);

% for k = 1:length(tifFiles)
%     filename = tifFiles(k).name;
%     I = imread(filename);
%     x = [0 10 100 1000 8000];
%     y = [0 10 100 1000 8000];
%     improfile(I,x,y),grid on;
%     figure('visible','off')
%     %set(gcf,'Visible','off')
%     %saveas(gcf,'pattern','tiff')
%     %close(gcf)
% end

%set(gcf,'Visible','off')
for j = 1;
    filename = tifFiles(j).name;
    I = imread(filename);
    image(I);
    colormap(jet(250));
    figure('visible','off');
    %filename = sprintf('pattern%d.tif',j);
    saveas(j,'colormap','tiff');
    %saveas(j,'pattern.tif')
end

toc
%saveas(j,'pattern.tif')
%A = imread('pattern1.tif');
%B = imread('pattern2.tif');
%imshowpair(A,B,'diff');
%%Create for loop to select all images, generate color map, and intensity plots

%I = ad1769_1:ad1769_2500
%I = imread('ad1769_raster3_0.00001');
%Creating intensity plot (Intentsity of each pixel in the 2048*2048)
%Assigning line segement coordinates for 3D representaion of intensity\
%x = [19 427 416 77];
%y = [96 462 37 33];
%x = [0 10 100 1000 8000];
%y = [0 10 100 1000 8000];
%Create grid of image using assigned coordinates
%improfile(I,x,y),grid on;
%Create intensity plot
%figure ()
%Commented ginput interface button to get desired coordinate
%[x1,y1] = ginput(1)
%Create colormap
%figure ()
%image(I)
%colormap jet