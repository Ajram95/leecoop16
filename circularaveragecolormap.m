%Colormap of circular average intensities for tissue
cd('C:\Users\jiliang liu\Desktop\Arjun\LeeLab-Coop1\human brain tissue')
load new_sub_bkgd1.mat

C = nanmean(I_tissue);
grid = reshape(C,[50,50]);
figure (1)
plot(I_tissue)

figure (2)
imagesc(grid)
colormap(jet(300))
title('50x50 view of average tissue intensity of each pattern from 2500 patterns (AD1769 human brain tissue)')
xlabel('Number of patterns')
hold on
colorbar('eastoutside')

figure (3)
plot(I_tissue(530:550,1:2500))


figure (4)
D = nanmean(I_tissue(530:550,1:2500));
grid1 = reshape(D,[50,50]);
imagesc(grid1)
colormap(jet(300))
title('Between 530 - 550 intensity points')
xlabel('Number of patterns')
hold on
colorbar('eastoutside')

% figure (4)
% E = nanmean(secondpeak);
% grid2 = reshape(E,[50,50]);
% imagesc(grid2)
% colormap(jet(1000))
% title('Between 520 - 620 intensity points')
% xlabel('Number of patterns')
% hold on
% colorbar('eastoutside')

