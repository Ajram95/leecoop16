r3%Colormap of circular average intensities for Amyloid only
cd('E:\LeeLab-Coop1\human brain tissue')
load further_bkgd_procs.mat

figure(1)
plot(I3)
title('Intensity of all 2500 patterns through 1000 points (Amyloid reflection)')
xlabel('Intensity points')
ylabel('Intensity')

C = nanmean(I3);
grid = reshape(C,[50,50]);
figure (2)
imagesc(grid)
colormap(jet)
title('Circular average intensity of Amyloid reflection for each of 2500 patterns')
xlabel('Number of patterns')
ylabel('Number of patterns')
hold on
colorbar('eastoutside')

figure(3)
D = nanmean(I_tissue(30:100,1:2500));
grid1 = reshape(D,[50,50]);
imagesc(grid1)
colormap(jet)
title('Between 30-100 pixels')
xlabel('Number of patterns')
ylabel('Number of patterns')
hold on
colorbar('eastoutside')



E = nanmean(I_tissue(460:490,1:2500));
grid2 = reshape(E,[50,50]);
figure (4)
imagesc(grid2)
colormap(jet(300))
title('Between 460-490 pixels')
xlabel('Number of patterns')
ylabel('Number of patterns')
hold on
colorbar('eastoutside')

F = nanmean(I_tissue(530:550,1:2500));
grid3 = reshape(F,[50,50]);
figure (5)
imagesc(grid3)
colormap(jet(300))
title('Between 530-550 pixels')
xlabel('Number of patterns')
ylabel('Number of patterns')
hold on
colorbar('eastoutside')

G = nanmean(I_tissue(590:610,1:2500));
grid4 = reshape(G,[50,50]);
figure (6)
imagesc(grid4)
colormap(jet(300))
title('Between 590-610 pixels')
xlabel('Number of patterns')
ylabel('Number of patterns')
hold on
colorbar('eastoutside')

<<<<<<< HEAD
idx5 = kmeans(I3,5);
figure(7)
[silh5,h] = silhouette(I3,idx5);
h = gca;
h.Children.EdgeColor = [.8 .8 1];
=======
% X = I_tissue;
% X = magic(5);
% c = clusterdata(X,'linkage','ward','savememory','on','maxclust',5);
% scatter(X(:,1),X(:,2),X(:,3),X(:,4),X(:,5),10,c)

>>>>>>> 5f8f5f2e60a9d864e306421c7dbcab4297cd371e

