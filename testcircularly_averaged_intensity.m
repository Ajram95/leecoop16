%clear;clc
tic

%cd('E:\LeeLab-Coop1\6_21_2016 Run\1824ac2_2\test 5')
%d=dir('testraw')

[X,Y]=meshgrid(1:2463,1:2527);
xpixel = 0.172; % X pixel size, in mM
ypixel = 0.172; % Y pixel size, in mM
%s2d = 416; % Sample-to-detector distance, in mM/2014 march brain sample s2d is 416mm
s2d = 400;% Sample-to-detector distance
lambda = 1.033; % Wavelength, in angstroms
xcenter = 1242; % X pixel coordinate of direct beam
ycenter = 1290; % Y pixel coordinate of direct beam

pfactor = 1; % polarization factor

max_angle = atan(200/s2d)*180/pi; % degrees, detector is 300x300 mm,therefore, here is 150/s2d.
scan_bins = 1000; % number of bins in 2-theta scan
two_theta_max = linspace(0,max_angle,scan_bins+1);
two_theta_max = two_theta_max(2:end);
two_theta_sp = two_theta_max(1); % spacing between bins
two_theta_center = two_theta_max - two_theta_sp/2;

two_theta = @(x,y) 180/pi*atan(sqrt(((x-xcenter)*xpixel).^2 + ...
    ((y-ycenter)*ypixel).^2)/s2d); % scatter angle, in degrees
% Counter-clockwise from beam stop
azimuth = @(x,y) atan((y-ycenter)./(x-xcenter)) + (x<xcenter).*pi + ((x>=xcenter)&(y<ycenter)).*2*pi;

two_theta_grid = two_theta(X,Y);
two_theta_ind = round(two_theta_grid/two_theta_sp)+1;
two_theta_ind(two_theta_ind>scan_bins)=1;
azimuthal_grid = azimuth(X,Y).*180/pi;
azimuthal_ind = round(azimuthal_grid)+1;

onetile = zeros(195,487);
for m = 1:7
onetile(:,(m*58+(m-1)*3):(m*58+(m-1)*3)+2) = 1;
end
onetile(96:98,:) = 1;
onetile(1:3,:) = 1;
onetile(end-2:end,:) = 1;
onetile(:,1:3) = 1;
onetile(:,end-2:end) = 1;
Alltiles = zeros(2527,2463);
for m = 1:5
for j = 1:12
Alltiles((j-1)*212+1:(j-1)*212+195,(m-1)*494+1:(m-1)*494+487) = onetile;
end
end
%imagesc(Alltiles)
D = imcomplement(Alltiles);

%parpool(4) %%here parallel computing doesn't show advantage on for
for i=1:46,
%      temp=readcbfgixsdata(i);
%      im=temp.RawData;
%      im=double(im);
    load(num2str(i), 'im');
    im = im.*D;
    
    X1=(im>40|im<1|two_theta_ind<=100);
    se=strel('square',1);
    X1=imdilate(X1,se);
    part1=(two_theta_ind<=180); 
    X1=X1-part1;
    X1=(X1==1);
    %figure
    X2=(im>220|im<1|two_theta_ind>200);
    se=strel('square',1);
    X2=imdilate(X2,se);
    part2=(two_theta_ind>186);
    X2=X2-part2;
    X2=(X2==1);
    mask=X1+X2;
    mask = mask|(two_theta_grid>max_angle);
    mask1x = [0];
    mask1y = [0];
    mask = mask;
    %imagesc(mask)
    pad_zero = @(x) [x; zeros(scan_bins-size(x,1),1)];
    
    counts = pad_zero(accumarray(two_theta_ind(~mask),1));
    pat_uncorr = pad_zero(accumarray(two_theta_ind(~mask),im(~mask)))./counts;
    
    I(:,i)=pat_uncorr(1:1000);

end
toc
r1=2*sin(two_theta_center.*pi/360)/1.033;
%save('circle_ave_intensity_S_new','I','r1') %%